#!/usr/bin/env node

var MongoClient = require('mongodb').MongoClient;
var diff = require('diff');
var phantomjs = require('phantomjs-prebuilt')
var webdriverio = require('webdriverio')
var wdOpts = { desiredCapabilities: { browserName: 'phantomjs' } }

var config = require('./config');
var journalSettings = config.journalSettings;
var journalDb = journalSettings['aging'].dbUrl;
var articleIds = [];
var localSource;
var liveSource;

MongoClient.connect(journalDb, function(db_err, db) {
        if(db_err) { cb(db_err); return; }

        var agingColl = db.collection('articles').find({"ids.pii":"101163"}, function(err, cursor){
                cursor.forEach(function(art) {
                        var article_id = art.ids.pii;

                        var localUrl = "http://localhost:3000/article/"+article_id+"/text";
                        var liveUrl = "http://www.aging-us.com/article/"+article_id+"/text";
                        console.log(localUrl+ " - " + liveUrl);


                        phantomjs.run('--webdriver=4444').then(program => {
                                webdriverio.remote(wdOpts).init()
                                .url(localUrl)
                                .waitForExist(".page-content", 20000)
                                .getHTML(".page-content")
                                .then(src => {
                                        localSource = src;
                                    })
                                .url(liveUrl)
                                .waitForExist(".page-content", 20000)
                                .getHTML(".page-content")
                                .then(src => {
                                        liveSource = src;
                                        console.log("Comparing HTML");
                                        diff.diffTrimmedLines(localSource, liveSource, function(err,res) {
                                                if(err) {
                                                    console.log("error: "+err);
                                                    program.kill();
                                                }
                                                else {
                                                    console.log(res);
                                                    program.kill();
                                                }
                                            });
                                    })
                            })
                    });
            });
    });

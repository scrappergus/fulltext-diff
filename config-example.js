var config = {};

config.journalSettings = {
	"aging": {
		"dbUrl": "mongodb://user:pass@host:port/db"
	},
	"oncotarget": {
		"dbUrl": "mongodb://user:pass@host:port/db"
	},
	"oncoscience": {
		"dbUrl": "mongodb://user:pass@host:port/db"
	},
	"genesandcancer": {
		"dbUrl": "mongodb://user:pass@host:port/db"
	}
}
config.live = false;
if(!config.live){
	config.journalSettings.aging.dbUrl = 'mongodb://localhost:3001/meteor';
	config.journalSettings.oncotarget.dbUrl = 'mongodb://localhost:3001/meteor';
	config.journalSettings.oncoscience.dbUrl = 'mongodb://localhost:3001/meteor';
	config.journalSettings.genesandcancer.dbUrl = 'mongodb://localhost:3001/meteor';
}

module.exports = config;
